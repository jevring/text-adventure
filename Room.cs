using System.Collections;
namespace DoD
{
	public class Room 
	{
		private string name;
	   private World world;
		private Hashtable exits;

		public Room()
		{
			exits = new Hashtable();
		}

		public string Name
		{
			get{return name;}
			set{name = value;}
		}

			public World World
		{
			get{return world;}
			set{world = value;}
		}

		public virtual void Enter(Player player)
		{
			if(player.NrOfVisits[this] == null)
			{
				player.Score++;
				player.NrOfVisits[this] = 1;
			}
			else
			{
				player.NrOfVisits[this]= (int)player.NrOfVisits[this] + 1;
			}

			player.World.TellPlayerAtRoomEnter(player,this);
		}
	
		public virtual void Leave(Player player)
		{
		}

		public virtual void SetExit(string direction, Room neighbor) 
		{
			exits[direction] =  neighbor;
		}

		public virtual Room GetExit(string direction)
		{
			return (Room) exits[direction];
		}

		public virtual void ShowExits(Player player)
		{
			string exitnames = " ";
			foreach(string key in exits.Keys)
				exitnames += key + " ";

			player.Tell(player.Trans("Exits:") +  exitnames);
		}
	}
}