namespace DoD
{
	public class WorldEx1 :World 
	{
		private Room room1;
		private Room room2;
		private Room roomGoal;
		private RoomEx1_nr3 room3;

		public WorldEx1(string name):base(name){}

		protected override void CreateWorldCommands()
		{
			CommandDelegate doGo = new CommandDelegate(DoGo);
			CommandDelegate doTake = new CommandDelegate(DoTake);
			CommandDelegate doOpen = new CommandDelegate(DoOpen);
			CommandDelegate doHelp = new CommandDelegate(DoCommandHelp);

			CreateCommandDef("take","take thing - Try to take a thing in this room",doTake, doHelp);
			CreateCommandDef("go","go exit - Try to leave this room through exit",doGo, doHelp);
			CreateCommandDef("open","open - Try to open a locked door",doOpen, doHelp);
		}
		protected override void CreateWorldRooms()
		{
			StartRoom  = AddRoom(new Room(),"Start room Ex1","startrum f�r Ex1");
			room1 = new Room();
			room2 = new Room();
			room3 = new RoomEx1_nr3();
			roomGoal = new Room();
			AddRoom(room1,"Room number one. There is a door in this room!","Rum ett. Det finns en d�rr i rummet!");
			AddRoom(room2,"Room number two","Rum tv�");
			AddRoom(room3,"Room number three","Rum tre");
			AddRoom(roomGoal,"The holy Grail room!","Skattkammaren!");
			AddLexicon("WelcomeEx1","Welcome to world Ex1","V�lkommen till v�rld Ex1");

			Connect(StartRoom, "e", room1,  "w");
			Connect(room1, "n", room2, "s");
			Connect(room1, "s", room3, "w");
			Connect(room2, "e", room3, "n");


													 }

		public override void Enter(Player player)
		{
			player.Tell("WelcomeEx1");
		}

		public void DoGo(Player player, Command cmd)
		{
			string exit = cmd[1];
			Room newroom = player.Room.GetExit(exit);
			if(newroom == null)
			{
				player.Tell("You can not go there");
				return;
			}
			player.Enter(newroom);
			return;
		}

		public void DoTake(Player player, Command cmd)
		{
			if(player.Room != room3)
			{
				player.Tell("There is nothing here to take");
				return;
			}
			room3.TryTakeKey(player,cmd[1]);
		}

		public void DoOpen(Player player, Command cmd)
		{
			if(player.Room != room1)
			{
				player.Tell("There is nothing here to open");
				return;
			}
			if(player.GetFromKnapsack("key") == null)
			{
				player.Tell("You don't have the key to the door");
				return;
			}
			player.Tell("The door is now open");
			Connect(room1,"door",roomGoal,"door");

		}

		private void Connect(Room room1,string door1, Room room2, string door2)
		{
			room1.SetExit(door1,room2);
			room2.SetExit(door2,room1);
		}
	
	}
}