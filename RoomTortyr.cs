using DoD;
namespace DoD.GruppB1
{

	public class RoomTortyr : RoomKS
	{
		bool key = true;
		public RoomTortyr (LexiconDelegate lex) : base(lex)
		{}

		public override void Enter(Player player)
		{
			base.Enter(player);
			
			string str = "\nAs you entering the torture camber you are touched by a gentle breeze with";
			str +=" a distinct smell of blood. The room is quiet and quite dark";
			str +=". The camber differs from the rest of the rooms in the castle, for once there isnt any ";
			str +="paint on the walls besides some blood stains here and there, and the floor is full";
			str +=" of dirt and filth.\nIn the center of the room you see a table with a drill and some cloth straps";
			str +=". In the back left corner lies what seems to be a human skeleton. ";
			str +="Its hands are strapped to a chain which is connected to some sort of metal wheel.";
			str +="On the left wall there is a metal cupboard, and under it there is a broken axe leaning up against the wall. ";
			str +="Your think for your self 'someone should really clean this place up'\n\n";
			str +="what do you want to do?\n\n";
			str +="solution: move/use wheel, take key, open cupboard, take ingrediance\n";
			
			player.Tell(str);
		}
	
		public void TryTakeKey(Player player, string what)
		{
			// First test if key can be taken
			if(what != "key")
			{
				player.Tell("There is no such thing here");
				return;
			}
			if(player.GetFromKnapsack("key") != null)
			{
				player.Tell("You already have the key!");
				return;
			}
			if(key == false)
			{
				player.Tell("The key is already taken! Sorry");
				return;
			}

			// take key and put in knapsack!
			key = false;
			player.PutInKnapsack("key",true);
		}
	}
}