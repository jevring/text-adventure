using System;
using DoD;

namespace DoD.GruppB1
{
	/// <summary>
	/// Summary description for 
	/// </summary>
	/// 
	public class WorldKalmarSlott : World
	{
		public LexiconDelegate ksAddLexicon;
		private RoomHall hall;
		private RoomBibliotek library;
		private RoomBalsal ballRoom;
		private RoomSlottskyrka church;
		private RoomMatsal diningHall;
		private RoomKok kitchen;
		private RoomTortyr tortureRoom;

		public WorldKalmarSlott(string name): base(name)
		{
			ksAddLexicon = new LexiconDelegate(base.AddLexicon);

			CreateKalmarSlottRooms();
		}

		protected override void CreateWorldCommands()
		{
			CommandDelegate doHelp = new CommandDelegate(base.DoCommandHelp);
			CommandDelegate doGo = new CommandDelegate(DoGo);
			CommandDelegate doDrop = new CommandDelegate(DoDrop);
			CommandDelegate doMove = new CommandDelegate(DoMove);
			CommandDelegate doOpen = new CommandDelegate(DoOpen);
			CommandDelegate doShow = new CommandDelegate(DoShow);
			CommandDelegate doTake = new CommandDelegate(DoTake);
			CommandDelegate doTalk = new CommandDelegate(DoTalk);			
			CommandDelegate doUse = new CommandDelegate(DoUse);
			CommandDelegate doScore = new CommandDelegate(DoScore);
			CommandDelegate doStat = new CommandDelegate(DoStat);
			CommandDelegate doLang = new CommandDelegate(DoLang);

			CreateCommandDef("go","HGo",doGo, doHelp);
			CreateCommandDef("drop","HDrop",doDrop, doHelp);
			CreateCommandDef("move","HMove",doMove, doHelp);
			CreateCommandDef("open","HOpen",doOpen, doHelp);
			CreateCommandDef("show","HShow",doShow, doHelp);
			CreateCommandDef("take","HTake",doTake, doHelp);
			CreateCommandDef("talk","HTalk",doTalk, doHelp);
			CreateCommandDef("use","HUse",doUse, doHelp);
			CreateCommandDef("score","HScore",doScore);
			CreateCommandDef("stat","HStat",doStat);
			CreateCommandDef("lang","HLang",doLang);

			AddSynonym("go","g�");
			AddSynonym("drop","sl�pp");
			AddSynonym("move","flytta");
			AddSynonym("open","�ppna");
			AddSynonym("show","visa");
			AddSynonym("take","ta");
			AddSynonym("talk","tala");
			AddSynonym("use","anv");
			AddSynonym("lang","spr�k");
			AddSynonym("score","po�ng");

			AddLexicon("HGo","go exit - Try to leave this room through exit",
				"g� utg�ng - F�rs�k att l�mna rummet genom utg�ng");
			AddLexicon("HDrop","drop thing - Try to drop a thing that earlier was taken in this room",
				"sl�pp sak - F�rs�k att sl�ppa en sak som tidigare tagists i rummet");
			AddLexicon("HMove","move thing - Try to move a thing in this room",
				"flytta sak - F�rs�k att flytta en sak i detta rum");
			AddLexicon("HOpen","open thing - Try to open something","�ppna sak - F�rs�k att �ppna n�got");
			AddLexicon("HShow","show (thing) - To show what you have in the knapsack or to show a thing",
				"visa (sak)- Att visa vad som finns i din ryggs�ck eller att visa en sak");
			AddLexicon("HTake","take thing - Try to take a thing in this room",
				"ta sak - F�rs�k att ta en sak i detta rum"); 
			AddLexicon("HTalk","talk - Try to talk to somebody.",
				"tala - F�rs�k att tala med n�gon."); 
			AddLexicon("HUse","use thing - Try to use a thing in som way",
				"anv sak - F�rs�k att anv�nda en sak p� n�got s�tt");
			AddLexicon("HLang","Change to language e(english) or s (swedish)",
				"Byt spr�k till e(engelska) eller s(svenska).");
			AddLexicon("HScore","Show your current score","Visa dina po�ng!");
			AddLexicon("HStat","Show statistics from visited worlds and room.", 
				"Visa statistik f�r bes�kta v�rldar och rum.");
		}
		protected override void CreateWorldRooms()
		{
			// Room created by World is instantiated before the contstructor in this class is done!
			hall = null;
		}
		protected void CreateKalmarSlottRooms()
		{
			hall = new RoomHall(ksAddLexicon);
			library = new RoomBibliotek(ksAddLexicon);
			ballRoom = new RoomBalsal(ksAddLexicon);
			church = new RoomSlottskyrka(ksAddLexicon);
			kitchen = new RoomKok(ksAddLexicon);
			diningHall = new RoomMatsal(ksAddLexicon);
			tortureRoom = new RoomTortyr(ksAddLexicon);

			StartRoom = hall;
				
			AddRoom(hall,"RoomHall");
			AddRoom(library,"RoomBibliotek");
			AddRoom(ballRoom,"RoomBalsal");
			AddRoom(church,"RoomSlottskyrka");
			AddRoom(kitchen,"RoomKok");
			AddRoom(diningHall, "RoomMatsal");
			AddRoom(tortureRoom,"RoomTortyr"); 
			
			AddLexicon("WelcomeKS","Welcome to Calmar Castle! Try to figure out what happened one of the masters of this castle, long time ago.",
				"V�lkommen till Kalmar Slott! F�rs�k att lista ut vad som h�nde en av slottsherrarna f�r l�nge sedan.");
			AddLexicon("CreatorKS","All future incomes belong to B1, Uppsala University.",
				"Alla framtida inkomster tillh�r B1, Uppsala Universitet.");

			AddLexicon("NoDoor","You can not go there. It's not allowed!","Du kan inte g� dit! Det �r f�renat med livsfara!");
			AddLexicon("NoDrop","There is nothing to drop!","Det finns inget att sl�ppa!");
			AddLexicon("NoMove","There is nothing here to move, you see.","Det finns inget att flytta!");
			AddLexicon("NoOpen","There is nothing to open!","Det finns inget att �ppna!");
			AddLexicon("NoShow","There is nothing here to show for you.","Det finns inget att visa!");
			AddLexicon("NoTake","There is nothing here to take.","Det finns inget att ta!");
			AddLexicon("NoTalk","There is nobody here to talk to!","Det finns ingen h�r att tala med!");
			AddLexicon("NoUse","There is nothing here to use.","Det finns inget att anv�nda!");
		
			Connect(hall, "ne", library,  "w");
			Connect(hall, "se", church,  "w");
			Connect(hall, "sw", diningHall,  "e");
			Connect(hall, "nw", ballRoom, "e");
			Connect(ballRoom, "s", diningHall, "n");
			Connect(kitchen, "n", diningHall, "s");
			Connect(kitchen, "e", tortureRoom, "w");
			Connect(church, "s", tortureRoom, "n");
		}
		private void Connect(Room room1,string door1, Room room2, string door2)
		{
			room1.SetExit(door1,room2);
			room2.SetExit(door2,room1);
		}

		public override void Enter(Player player)
		{
			player.Tell("WelcomeKS");
			player.Tell("CreatorKS");
		}

		protected  void DoScore(Player player, Command cmd)
		{
			player.Tell("Score = " + player.Score);
		}
		protected void DoLang(Player player, Command cmd)
		{
			if(cmd[1] == "e") player.Language = "e";
			else if(cmd[1] == "s") player.Language = "s";
		}
		protected  void DoStat(Player player, Command cmd)
		{
			int i = 0;
			foreach(World world in Game.WorldList)
			{
				player.Tell(i.ToString() + " " + world.Name);
				i++;  //Teleporteringen l�gger ut med data index!!!
				foreach (Room room in world.RoomList)
				{
					if(player.NrOfVisits[room] != null)
					{
						player.Tell("   " + player.NrOfVisits[room] + " " + player.Trans(room.Name));
					}
				}

			}
		}
		protected  void DoGo(Player player, Command cmd)
		{
			string exit = cmd[1];
			Room newroom = player.Room.GetExit(exit);
			if(newroom == null)
			{
				player.Tell("NoDoor");
				return;
            }
			player.Enter(newroom);
			return;
		}

		/*
		 * Local methods in the rooms
		 *
		 */

		protected void DoDrop(Player player, Command cmd)
		{
			RoomKS playerKSRoom = (RoomKS) player.Room;
			playerKSRoom.DoDrop(player, cmd);
			return;			
		}
		protected void DoMove(Player player, Command cmd)
		{
			RoomKS playerKSRoom = (RoomKS) player.Room;
			playerKSRoom.DoMove(player, cmd);
			return;			
		}
		protected void DoOpen(Player player, Command cmd)
		{
			RoomKS playerKSRoom = (RoomKS) player.Room;
			playerKSRoom.DoOpen(player, cmd);
			return;
		}
		protected void DoShow(Player player, Command cmd)
		{
			RoomKS playerKSRoom = (RoomKS) player.Room;
			playerKSRoom.DoShow(player, cmd);
			return;			
		}
		protected void DoTake(Player player, Command cmd)
		{
			RoomKS playerKSRoom = (RoomKS) player.Room;
			playerKSRoom.DoTake(player, cmd);;
			return;			
		}
		protected void DoTalk(Player player, Command cmd)
		{
			RoomKS playerKSRoom = (RoomKS) player.Room;
			playerKSRoom.DoTalk(player, cmd);
			return;			
		}
		protected void DoUse(Player player, Command cmd)
		{	
			RoomKS playerKSRoom = (RoomKS) player.Room;		
			playerKSRoom.DoUse(player, cmd);
			return;			
		}
	
	}
}
