namespace DoD
{
	public class WorldAtStart : World 
	{
		public WorldAtStart(string name):base(name){}

		protected override void CreateWorldCommands()
		{
			CommandDelegate doScore = new CommandDelegate(DoScore);
			CreateCommandDef("score","HScore",doScore);
			CommandDelegate doStat = new CommandDelegate(DoStat);
			CreateCommandDef("stat","HStat",doStat);
			CommandDelegate doLang = new CommandDelegate(DoLang);
			CreateCommandDef("lang","HLang",doLang);

			AddLexicon("HLang","Change to language e(english) or s (swedish)",
				"Byt spr�k till e(engelska) eller s(svenska).");
			AddLexicon("HScore","Show your current score","Visar upp dina po�ng!");
			AddLexicon("HStat","Show statistics", "Visa statistik bes�kta v�rldar och rum.");
			AddLexicon("RName","a very dark cellar","en kolsvart k�llare");

			AddSynonym("lang","spr�k");
			AddSynonym("score","po�ng");
		}

		protected override void CreateWorldRooms()
		{
			StartRoom =AddRoom(new Room(),"RName");
		}

		protected  void DoScore(Player player, Command cmd)
		{
			player.Tell("Score = " + player.Score);
		}
		protected void DoLang(Player player, Command cmd)
		{
			if(cmd[1] == "e") player.Language = "e";
			else if(cmd[1] == "s") player.Language = "s";
		}

		protected  void DoStat(Player player, Command cmd)
		{
			foreach(World world in Game.WorldList)
			{
				player.Tell(world.Name);
				foreach (Room room in world.RoomList)
				{
					if(player.NrOfVisits[room] != null)
					{
						player.Tell("   " + player.NrOfVisits[room] + " " + player.Trans(room.Name));
					}
				}

			}
		}
	}
}