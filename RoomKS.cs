using System;

namespace DoD.GruppB1
{
	/// <summary>
	/// RoomKS is a pattern room for KalmarSlott.
	/// All available commands are built as virtual with the not used message.
	/// In the real room you must make overrides of used commands.
	/// </summary>
	/// 
	public delegate void LexiconDelegate(string key,string english,string swedish);
	public class RoomKS : Room
	{
		protected LexiconDelegate ksAddLexicon;
		public RoomKS(LexiconDelegate lex)
		{
			ksAddLexicon = lex;
			ksAddLexicon("knapsack","The knapsack contains: ","Rycks�cken inneh�ller: ");
			ksAddLexicon("recipe","recipe pea soup","recept �rtsoppa");
			ksAddLexicon("peas","peas","�rtor");
			ksAddLexicon("pork","pork","fl�sk");
			ksAddLexicon("holywater","holy water","vigt vatten");
			ksAddLexicon("thyme","thyme","timjan");
		}
		
		public virtual void DoDrop(Player player, Command cmd)
		{
			player.Tell("NoDrop");
			return;			
		}
		public virtual void DoMove(Player player, Command cmd)
		{
			player.Tell("NoMove");
			return;			
		}
		public virtual void DoOpen(Player player, Command cmd)
		{
			
			player.Tell("NoOpen");
			return;
		}
		public virtual void DoShow(Player player, Command cmd)
		{
			if (cmd[1] == "")
			{
				player.Tell("knapsack");
				if(player.GetFromKnapsack("recipe") != null)
					player.Tell("recipe");
				if(player.GetFromKnapsack("peas") != null)
					player.Tell("peas");
				if(player.GetFromKnapsack("pork") != null)
					player.Tell("pork");
				if(player.GetFromKnapsack("holywater") != null)
					player.Tell("holywater");
				if(player.GetFromKnapsack("thyme") != null)
					player.Tell("thyme");
				if(player.GetFromKnapsack("salt") != null)
					player.Tell("salt");
				return;
			}
			player.Tell("NoShow");
			return;			
		}
		public virtual void DoTake(Player player, Command cmd)
		{
			player.Tell("NoTake");
			return;			
		}
		public virtual void DoTalk(Player player, Command cmd)
		{
			player.Tell("NoTalk");
			return;			
		}
		public virtual void DoUse(Player player, Command cmd)
		{
			player.Tell("NoUse");
			return;			
		}
	}
}
