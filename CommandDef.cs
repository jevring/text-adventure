namespace DoD
{
	public class CommandDef 
	{
		public string Name;
		public string Help;
		public CommandDelegate DoCommand;
		public CommandDelegate DoHelp;
	
		public CommandDef(string name, string help, CommandDelegate del, CommandDelegate delhelp)
		{
			Name = name;
			Help = help;
			DoCommand = del;
			DoHelp = delhelp;
		}
	
}
}
