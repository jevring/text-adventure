using DoD;
namespace DoD.GruppB1

{

	public class RoomBibliotek : RoomKS
	{
		static string enBook1 = "CookbookOfCajsaWarg";
		static string enBook2 = "HerbariumOfCarlvonLinn�";
		static string enBook3 = "RulesOfTheRoyalAncientGolfClubOfStAndrews";
		static string enBook4 = "NotesOfKarin";
		static string swBook1 = "CajsaWargsKokbok";
		static string swBook2 = "CarlvonLinn�sHerbarium";
		static string swBook3 = "RulesOfTheRoyalAncientGolfClubOfStAndrews";
		static string swBook4 = "AnteckningarAvKarin";

		private bool ladderMoved;
		private bool uponLadder;

		public RoomBibliotek (LexiconDelegate lex) : base(lex)
		{
			ksAddLexicon("RoomBibliotek","the old, dusty library. On the shelves there are books as: " + enBook1 + ", " + enBook2 + ", " + enBook3 + ", " + enBook4 + ".  There are chairs, a table and a ladder made in old oak. Room created by Mona Carlsson.",
				"det gamla, dammiga biblioteket. P� hyllorna finns b�cker som: " + swBook1 + ", " + swBook2 + ", " + swBook3 + ", " + swBook4 + ". Det finns stolar, ett bord och en stege, allt i gammal ek. Skapat av Mona Carlsson.");
			ksAddLexicon("mcLadder","The ladder is moved.","Stegen har flyttats.");
			ksAddLexicon("mcChair","The chair is moved.","Stolen har flyttats.");
			ksAddLexicon("mcTable","The table is moved.","Bordet har flyttats.");
			ksAddLexicon("mcNoMove","What do you want to move?","Vad vill du flytta?");
			ksAddLexicon("mcLadder2","You have climbed the ladder.","Du har kl�ttrat upp p� stegen.");
			ksAddLexicon("mcChair2","You are sitting in a chair.","Du sitter p� en stol.");
			ksAddLexicon("mcTable2","You are sitting at the table.","Du sitter vid bordet.");
			ksAddLexicon("mcNoUse","What do you want to use?","Vad vill du anv�nda?");
			ksAddLexicon("mcNotReachable","You can't reach the book.","Du kan inte n� boken.");
			ksAddLexicon("mcBook1","You take what you have.","Man tager vad man haver.");
			ksAddLexicon("mcBook2","Allium schoenoprasum, Mentha peperita, Thymus vulgaris, Tropaeolum majus",
					"Allium schoenoprasum, Mentha peperita, Thymus vulgaris, Tropaeolum majus.");
			ksAddLexicon("mcBook3","Rules and decisions of golf.","Regler och beslut f�r golf.");
			ksAddLexicon("mcBook4","Erik's pea soup: peas, pork, holy water, thyme and salt.",
				"Eriks �rtsoppa: �rtor, fl�sk, vigt vatten, timjan och salt.");
			ksAddLexicon("mcNoBook","That book is missing.","Den boken finns inte.");
			ksAddLexicon("mcWrongBook","Wrong book. You loose score!","Fel bok. Du f�rlorar po�ng!");
			ksAddLexicon("mcNotTheBook","You have to choose one spice!","Du m�ste v�lja en kryddv�xt!");
			ksAddLexicon("mcTakeBook4","The recipe is put in the knapsack.","Receptet har stoppats i ryggs�cken.");
			ksAddLexicon("mcNoTake","You can't take that!","Detd�r kan du inte ta med dig!");
			ksAddLexicon("mcWrongHerbe","Wrong spice. You loose some score!","Fel kryddv�xt. Du f�r b�ta n�gra po�ng!");
			ksAddLexicon("mcTakeThyme","Thyme is put in the knapsack.","Timjan har stoppats i ryggs�cken.");
			ksAddLexicon("mcIsTaken","You allready have this thing in your knapsack.","Denh�r saken finns redan i din ryggs�ck.");
			ksAddLexicon("mcNoOpen","What to open?","Vad ska �ppnas?");
		}

		public override void Enter(Player player)
		{
			
			ladderMoved = false;
			uponLadder = false;
	/* Ta bort
			player.PutInKnapsack("peas","peas");
			player.PutInKnapsack("pork","pork");
			player.PutInKnapsack("holywater","holywater");
			player.PutInKnapsack("salt","salt");
	*/ 
			base.Enter(player);
		}

		public override void DoMove(Player player, Command cmd)
		{
			if (cmd[1] == "ladder" || cmd[1] == "stege")
			{
				player.Score += 1;
				if (ladderMoved)
					ladderMoved = false;					
				else
					ladderMoved = true;
				player.Tell("mcLadder");
				return;
			}
			else if (cmd[1] == "chair" || cmd[1] == "stol")
			{
				player.Tell("mcChair");
				return;
			}
			else if (cmd[1] == "table" || cmd[1] == "bord")
			{
				player.Tell("mcTable");
				return;
			}
			player.Tell("mcNoMove");
			return;
		}
		public override void DoUse(Player player, Command cmd)
		{
			if (cmd[1] == "ladder" || cmd[1] == "stege")
			{
				uponLadder = true;
				player.Tell("mcLadder2");
				return;
			}
			if (cmd[1] == "table" || cmd[1] == "bord")
			{
				uponLadder = false;
				player.Tell("mcTable2");
				return;
			}
			if (cmd[1] == "chair" || cmd[1] == "stol")
			{
				uponLadder = false;
				player.Tell("mcChair2");
				return;
			}
			player.Tell("mcNoUse");
			return;
		}
		public override void DoShow(Player player, Command cmd)
		{
			if (cmd[1] == "")
			{
				base.DoShow (player, cmd);
			}
			else if (cmd[1] == enBook1 || cmd[1] == swBook1)
			{
				if (ladderMoved && uponLadder)
				{
					player.Tell("mcBook1");
					return;
				}
				else
				{
					player.Tell("mcNotReachable");
					return;
				}
			}
			else if (cmd[1] == enBook2 || cmd[1] == swBook2)
			{
				if (ladderMoved && uponLadder)
				{
					player.Tell("mcBook2");
					return;
				}
				else
				{
					player.Tell("mcNotReachable");
					return;
				}
			}
			else if (cmd[1] == enBook3 || cmd[1] == swBook3)
			{
				if (ladderMoved && uponLadder)
				{
					player.Tell("mcBook3");
					return;
				}
				else
				{
					player.Tell("mcNotReachable");
					return;
				}
			}
			else if (cmd[1] == enBook4 || cmd[1] == swBook4)
			{
				if (ladderMoved && uponLadder)
				{
					player.Tell("mcBook4");
					return;
				}
				else
				{
					player.Tell("mcNotReachable");
					return;
				}
			}
			else 
			{
				player.Tell("mcNoBook");
				return;
			}
		}

		public override void DoOpen(Player player, Command cmd)
		{
			if (cmd[1] == enBook1 || cmd[1] == swBook1)
			{
				if (ladderMoved && uponLadder)
				{
					player.Tell("mcBook1");
					return;
				}
				else
				{
					player.Tell("mcNotReachable");
					return;
				}
			}
			else if (cmd[1] == enBook2 || cmd[1] == swBook2)
			{
				if (ladderMoved && uponLadder)
				{
					player.Tell("mcBook2");
					return;
				}
				else
				{
					player.Tell("mcNotReachable");
					return;
				}
			}
			else if (cmd[1] == enBook3 || cmd[1] == swBook3)
			{
				if (ladderMoved && uponLadder)
				{
					player.Tell("mcBook3");
					return;
				}
				else
				{
					player.Tell("mcNotReachable");
					return;
				}
			}
			else if (cmd[1] == enBook4 || cmd[1] == swBook4)
			{
				if (ladderMoved && uponLadder)
				{
					player.Tell("mcBook4");
					return;
				}
				else
				{
					player.Tell("mcNotReachable");
					return;
				}
			}
			else 
			{
				player.Tell("mcNoOpen");
				return;
			}
		}

		public override void DoTake(Player player, Command cmd)
		{
			if (cmd[1] == enBook1 || cmd[1] == swBook1)
			{
				if (ladderMoved && uponLadder)
				{
					player.Score -= 8;
					player.Tell("mcWrongBook");
					return;
				}
				else
				{
					player.Tell("mcNotReachable");
					return;
				}
			}
			else if (cmd[1] == enBook2 || cmd[1] == swBook2)
			{
				if (ladderMoved && uponLadder)
				{
					player.Tell("mcNotTheBook");
					return;
				}
				else
				{
					player.Tell("mcNotReachable");
					return;
				}
			}
			else if (cmd[1] == enBook3 || cmd[1] == swBook3)
			{
				if (ladderMoved && uponLadder)
				{
					player.Score -= 10;
					player.Tell("mcWrongBook");
					return;
				}
				else
				{
					player.Tell("mcNotReachable");
					return;
				}
			}
			else if (cmd[1] == enBook4 || cmd[1] == swBook4)
			{
				if (ladderMoved && uponLadder)
				{
					if(player.GetFromKnapsack("recipe") != null)
					{
						player.Tell("mcIsTaken");
						return;
					}
					else
					{
						player.Score += 7;
						player.PutInKnapsack("recipe",true);
						player.Tell("mcTakeBook4");
						return;
					}
				}
				else
				{
					player.Tell("mcNotReachable");
					return;
				}
			}
			else if (cmd[1] == "Allium" && cmd[2] == "schoenoprasum" || 
					 cmd[1] == "Mentha" && cmd[2] == "peperita" ||
					 cmd[1] == "Tropaeolum" && cmd[2] == "majus" )
			{
				player.Score -= 2;
				player.Tell("mcWrongHerbe");
				return;
			}
			else if (cmd[1] == "Thymus" && cmd[2] == "vulgaris")
			{
				if(player.GetFromKnapsack("thyme") != null)
				{
					player.Tell("mcIsTaken");
					return;
				}
				else
				{
					player.Score += 8;
					player.PutInKnapsack("thyme",true);
					player.Tell("mcTakeThyme");
					return;
				}
			}
			else 
			{
				player.Tell("mcNoTake");
				return;
			}
		}
	}
}