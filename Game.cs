using System.Collections;
namespace DoD
{
	public class Game 
	{
		public static ArrayList WorldList;
		public static bool StillPlaying;
	
		private static ArrayList PlayerList;
		private static World StartWorld;
	
		public Game()
		{
			PlayerList = new ArrayList();
			WorldList = new ArrayList();
			/*
			StartWorld = new WorldAtStart("Start world");
			AddWorld(StartWorld);
			AddWorld(new WorldEx1("Example 1 World"));
			AddWorld(new WorldEx2("Example 2 World"));
			*/
			AddWorld(new GruppB1.WorldKalmarSlott("monkey!"));
			AddPlayer(new Player("KA", "Kalle Andersson"));	
	//		AddPlayer(new Player("PP", "Pelle Persson"));
		}
	
		public static void AddPlayer(Player player)
		{
			PlayerList.Add(player);
			player.Enter(StartWorld); 
		
		}
	
	
		private void AddWorld(World world)
		{
			WorldList.Add(world);
		}
	
		public void StartGame()
		{
			StillPlaying = true;

			while(StillPlaying)
			{
            int nrOfLivingPlayers = 0;
				foreach(Player player in PlayerList)
				{
					World CurrentWorld = player.World;    // World property
					if(player.IsAlive)
					{	
						nrOfLivingPlayers++;
						while(!CurrentWorld.ExecuteNextCommand(player)){};
					}
				}

				if(nrOfLivingPlayers == 0) return;
			}
		}
	}
}
	
	
