using System.Collections;
using DoD;
/** 
 * Room Kitchen was written by Peter Hultgren 2004
 * E-mail: pehu4941@student.uu.se
 */


namespace DoD.GruppB1 // S�h�r m�ste namespacen se ut enligt specifikationerna.
{
	public class RoomKok : RoomKS
	{
		public RoomKok (LexiconDelegate lex) : base(lex)
		{}
		private bool	peas = false,		// These denote whether the ingredient
			pork = false,		// has been put in the pot.
			holywater = false,		// They all must be there before the
			thyme = false,		// the cooking can start.
			salt = false,
			repaired = false,   // This one tells whether the fuse has been replaced
			triedCooking = false; // So the chef knows what to talk about

		
		private ArrayList pot;				// this is where the ingredients are put
					 
		public override void Enter(Player player)
		{
			base.Enter(player);
			// Check if all the necessary ingredients are brought.
			if(player.GetFromKnapsack("peas") != null)
				peas = true;
			if(player.GetFromKnapsack("pork") != null)
				pork = true;
			if(player.GetFromKnapsack("holywater") != null)
				holywater = true;
			if(player.GetFromKnapsack("thyme") != null)
				thyme = true;
			if(player.GetFromKnapsack("salt") != null)
				salt = true;

			if(peas && pork && holywater && thyme && salt) // If the ingredients are brought
			{
				if(player.Language.Equals("e"))
					player.Tell("You've brought all the ingredients. Your mission is done!");
				else
					player.Tell("Du har med dig alla ingredienser. Du har klarat niv�n!");
			}
			else // If some more ingredients need to be brought
			{
				if(player.Language.Equals("e"))
					player.Tell("You need more ingredients. Fetch them and come back!");
				else
					player.Tell("Du beh�ver mer ingredienser. H�mta dem och kom tillbaka!");
			}
		}

	}
}

