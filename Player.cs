using System;
using System.Collections;
namespace DoD
{
	public class Player 
	{
		private  int score;
		private  bool isAlive;
		private  Room  room;
		private  World world;
		private  string name;
		private  string nn;	
		private  Hashtable saveRoom;
		private  Hashtable nrofvisits;
		private  string language;
		private  Hashtable knapsack;

		public Player(string shortname, string fullname)
		{
			nn = shortname;
			name = fullname;
			isAlive = true;
			saveRoom = new Hashtable();	
			nrofvisits = new Hashtable();
			knapsack = new Hashtable();
			score = 0;
			language = "e";
		}

		public string Name
		{
			get{return Trans(name);}
		}
		public string NN
		{
			get{return nn;}
		}
		public string Language
		{
			get{return language;}
			set{language = value;}
		}
		
		public bool IsAlive
		{
			get{return isAlive;}
			set{isAlive = value;}
		}

		public int Score
		{
			get{return score;}
			set{score = value;}
		}

		public Room Room
		{
			get{return room;}
			set{room = value;}
		}
		public World World
		{
			get{return world;}
			set{world = value;}
		}
		
		public Hashtable NrOfVisits
		{
			get{return nrofvisits;}
		}

		public object GetFromKnapsack(object key)
		{
			if(knapsack[world] == null) return null;
			Hashtable worldKnapsack = (Hashtable) knapsack[world];
			return worldKnapsack [key];
		}
			
		public void PutInKnapsack(object key,object value)
		{
			if(knapsack[world] == null) knapsack[world] = new Hashtable();
			Hashtable worldKnapsack = (Hashtable) knapsack[world];
			worldKnapsack[key] = value;
		}

		public ParsedString Prompt(string p)
		{
			Console.Write(NN+p);
			String txt = Console.ReadLine();
			return Parse(txt);		
		}
		public string Trans(string msg)
		{
			return world.Translate(msg,language);
		}

		public void Tell(String msg)
		{
			Console.WriteLine(Trans(msg));
		}
	
		public void Enter(World newWorld)
		{
			// First check if the new world has a starting room
		
			Room newRoom = (Room)saveRoom[newWorld];   // a saved room?
			if(newRoom == null) newRoom = newWorld.StartRoom; // the start room
			if(newRoom == null)
			{
				Tell("WorldNoStartRoom");
				return;
			}

			if(World != null)
			{
				saveRoom[World] = Room;
				World.Leave(this);
				Room.Leave(this);
			}
		
			World = newWorld;
			Room = newRoom;
			World.Enter(this);
			Room.Enter(this);	
		}
	
		public void Enter(Room newRoom)
		{
			Room.Leave(this);
			Room = newRoom;
			Room.Enter(this);
		}

		private ParsedString Parse(string txt)
		{
			ParsedString ret;
			ret.SourceString = txt;

			char [] delimiter = new char[1];
			delimiter[0] = ' ';
			// Split string between blanks
			string [] split = txt.Split(delimiter);
			// Count number of separate words
			int n = 0;
			for (int i = 0; i < split.Length;i++)
				if(split[i] != "")n++;

			string [] words = new string[n];

			// Translate string to language in use
			n = 0;
			for(int i = 0; i < split.Length; i++)
			{
				if(split[i] != "")
				{
					words[n] = TransFromPlayer(split[i]);
					n++;
				}
			}
			ret.Word = words;
			return ret;


		}

		protected string TransFromPlayer(string word)
		{
			return world.TranslateFromPlayer(word,language);
		}
	}
}
