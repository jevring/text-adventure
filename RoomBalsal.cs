/**
 * Created By: Markus Jevring <maje4823@student.uu.se>
 */

namespace DoD.GruppB1
    {

	public class RoomBalsal : RoomKS
	{
		public RoomBalsal (LexiconDelegate lex) : base(lex)
		{
			string enEnterString = "As you enter the room, the muted sounds of a waltz reach your ears. " + 
									"Even though you can see no orchestra, you can sense that there used to be dancing here almost every night. " + 
									"There is a big, lit chandelier hanging from the ceiling, and the floor is shining and you can almost see your own reflection. " + 
									"Along the far wall is a long diningtable set for a massive 40 guests. Among the things serves is a giant side of pork. " + 
									"As you walk over the floor, you get a feeling that an old but samll grandfatherclock and a candlestick are watching you, " + 
									"but you shurg it off as you reach the middle of the room. You take a few steps and a swirl out on the floor and take a good look around. " + 
									"That pork is starting to smell mighty nice.";
			string svEnterString = "N�r du kommer in i rummet h�r du ljuva med tysta toner som l�ter som en walz. " + 
									"Trots att du inte ser n�gon orkester s� har du en k�nsla av att folk hade stora fester och dansade h�r n�stan varje kv�ll. " + 
									"Det h�nger en stor t�nd kristallkrona i taket, och golvet �r s� blankpolerat att du n�stan kan spegla dig i det. " + 
									"Vid l�ngsidan av rummet st�r ett l�ng middagsbord dukat f�r 40 g�ster. Bland dom olika matr�tterna ser du en stor stekt gris.";
			ksAddLexicon("RoomBalsal", enEnterString, svEnterString);
			// changed
			
			string enPickupPork = "You walk over to the dinner table. You pick up a knife and a fork and carv yourself a nice good side of pork and stuff it in your knapsack.";
			string svPickupPork = "Du g�r ��ver till middagsbordet, plockar upp kniv och gaffel och sk�r ut en stor fine bit fl�sk och stoppar den i ryggs�cken";
			ksAddLexicon("BallroomPickupPork", enPickupPork, svPickupPork);

			string enUseChair = "You give the chair a try. You pull it out and sit down. \"My, this is a comfy chair\" you exclaim. After really feeling its fabric, you get up and put the chair back where you found it";
			string svUseChair = "Du provar en stol. Du drar ut den och s�tter dig. \"Vilken bekv�m stol\". Efter att ha suttit ett tag och vilat benen s� st�ller du dig upp och st�ller tillbaka stolen";
			ksAddLexicon("BallroomUseChair", enUseChair, svUseChair);

			

		}

		public override void DoTake(Player player, Command cmd)
		{
			if ("pork".Equals(cmd[1]) || "fl�sk".Equals(cmd[1])) 
			{
				player.Tell("BallroomPickupPork");
				player.PutInKnapsack("pork", "A side of pork");
				player.Score++;
			}
			else
			{
				player.Tell("NoTake");
			}
		}
		public override void DoUse(Player player, Command cmd)
		{
			if ("chair".Equals(cmd[1]) || "stol".Equals(cmd[1])) 
			{
				player.Tell("BallroomUseChair");
			}
			else 
			{
				player.Tell("NoUse");
			}
		}
	}
}