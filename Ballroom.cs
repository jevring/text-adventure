using System;

namespace DoD.GruppB1
{
	/// <summary>
	/// Summary description for Balroom.
	/// </summary>
	public class Ballroom : Room
	{

		public Ballroom() : base() {}
		public override void Enter(Player player)
		{
			base.Enter (player);
			player.Tell("As you enter the room, the muted sounds of a waltz reach your ears. Even tho you can see no orchestra, you can sense that there used to be dancing here almost every night. There is a big, lit chandelier hanging from the ceiling, and the floor is shining and you can almost see your own reflection. Along the far wall is a long diningtable set for a massive 40 guests. Among the things serves is a giant side of pork. As you walk over the floor, you get a feeling that an old but samll grandfatherclock and a candlestick are watching you, but you shurg it off as you reach the middle of the room. You take a few steps and a swirl out on the floor and take a good look around. That pork is starting to smell mighty nice.");
		}
		public void PickupPork(Player player)
		{
			player.Tell("You walk over to the dinner table. You pick up a knife and a fork and carv yourself a nice good side of pork and stuff it in your knapsack.");
			player.PutInKnapsack("pork", "A nice big piece of pork.");
		}
		public void UseChair(Player player)
		{
			player.Tell("You give the chair a try. You pull it out and sit down. \"My, this is a comfy chair\" you exclaim. After really feeling its fabric, you get up and put the chair back where you found it");
		}


	}
}
