/**
 * Created By: Markus Jevring <maje4823@student.uu.se>
 */

namespace DoD.GruppB1
{

	public class RoomSlottskyrka : RoomKS
	{
		public RoomSlottskyrka (LexiconDelegate lex) : base(lex)
		{
			string enChurchEnter = "As you enter the church, you see the ceiling rise majestically over head, and the afternoon light falls in through the stained " + 
				"glass windows and makes traces through the airborn dust. The wooden pews are all a bit off, like they've been left in a hurry. " + 
				"To your side is a table with candles, two of which are lit. Down the aisle is the altar, upon which is set a bowl of holywater and some vials.";
			string svChurchEnter = "N�r du kliver in i slottskyrkan ser du hur taket tornar upp sig st�tligt ovanf�r digg huvud. Eftermiddagsljuset fallet brant in genom f�nstrena " + 
				"och g�r synliga sp�r genom dammet som h�nge ri luften i rummet. B�nkarna st�r lite halvsnett. Brevid dig finns ett bord med ett flertal ljus p�, tv� av dom �r t�nda. " + 
				"I slutet av mittg�ngen, p� altaret, st�r en dopfunt och n�gra sm� flaskor";
			ksAddLexicon("RoomSlottskyrka", enChurchEnter, svChurchEnter);
			
			string enHasVial = "The ghosts of this place will not allow you to take more than one vial from here. You put the vial back.";
			string svHasVial = "Du k�nner en isande k�nsla av att en flaska med vatten �r mer �n tillr�ckligt. Du st�llet tillbaka flaskan";
			ksAddLexicon("ChurchHasVial", enHasVial, svHasVial);

			string enTakeVial = "You approach the altar. Walking down the aisle you feel the hairs prickle on your neck and your footsteps echo through the entire church. " + 
				"Once you reach the altar you grab one of the vials. As you open it, you seem to hear a slight sigh and the sound of the wind outside. " + 
				"You dip the vial in the bowl and bring it back upp filled. You seal the vial and put it in your knapsack for later use.";
			string svTakeVial = "Du g�r fram till altaret. Du str�cker dig efter en av flaskorna, drar ur korken, doppar den i vattnet och fyller den. Sedan stoppar du tillbaka korken och l�gger ner den i din ryggs�ck.";
			ksAddLexicon("ChurchTakeVial", enTakeVial, svTakeVial);
		}

		public override void DoTake(Player player, Command cmd)
		{
			string item = cmd[1];
			switch(item) 
			{
				case "water" :
				case "holywater" :
				case "vatten" :
				case "vigvatten" :
				case "vial" : PickupVial(player); break;
				case "bibel" :
				case "bible" : PickupBible(player); break;
				case "ljus" :
				case "candle" : PickupCandle(player); break;
				default : PickupBible(player); break;
			}
		}

		public void PickupBible(Player player)
		{
			player.Tell("NoTake");
		}
		public void PickupCandle(Player player)
		{
			PickupBible(player);
		}
		public void PickupVial(Player player)
		{
			if (player.GetFromKnapsack("water") != null)
			{
				player.Tell("ChurchHasVial");
			}
			else
			{
				player.Tell("ChurchTakeVial");
				player.PutInKnapsack("holywater", "a vial filled with holywater");
				player.Score++;
			}
		}
		public override void DoUse(Player player, Command cmd)
		{
			DoTake(player, cmd);
		}
	}
}