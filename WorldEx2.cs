namespace DoD
{
	public class WorldEx2 : World 
	{
		public WorldEx2(string name):base(name){}

		protected override void CreateWorldCommands(){}

		protected override void CreateWorldRooms()
		{
			StartRoom = new Room();
			AddRoom(StartRoom, "start room for ex2", "startrum f�r ex2");
		}
		public override void Enter(Player player)
		{
			player.Tell("V�lkommen till svenska rummet Ex2");
		}
	}
}