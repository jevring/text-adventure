
namespace DoD
{

	public class RoomEx1_nr3 : Room
	{
		bool key = true;

		public override void Enter(Player player)
		{
			base.Enter(player);
			if(key)player.Tell("There is a small key hanging on the wall");
		}
	
		public void TryTakeKey(Player player, string what)
		{
			// First test if key can be taken
			if(what != "key")
			{
				player.Tell("There is no such thing here");
				return;
			}
			if(player.GetFromKnapsack("key") != null)
			{
				player.Tell("You already have the key!");
				return;
			}
			if(key == false)
			{
				player.Tell("The key is already taken! Sorry");
				return;
			}

			// take key and put in knapsack!
			key = false;
			player.PutInKnapsack("key",true);
		}
	}
		




}
