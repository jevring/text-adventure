using DoD;
namespace DoD.GruppB1
{

	public class RoomHall : RoomKS 
			
	{
		private int girlInRoom;
		private int totalHallVisits = 0;

		public RoomHall(LexiconDelegate lex) : base(lex)
		{
			ksAddLexicon("RoomHall","the cold mysterious Castle Hall with stone floor and filled with echo. Besides there is only oak-doors in this room. Created by Mona Carlsson.",
				"den kalla mystiska Slotts hallen med stengolv och fyllt av eko. F�r �vrigt finns det enbart ekd�rrar i detta rum. Skapat av Mona Carlsson.");
			ksAddLexicon("mcHallghost2","Haven�t I told you not to run here all day!",
				"Dig har jag best�mt sagt till tidigare, att inte springa h�r hela dagen!");
			ksAddLexicon("mcHallghost1","Don't touch me there. How dare you!","R�r mig inte! Hj�lp han �verfaller mig!");
			ksAddLexicon("mcHallghost0","What a awfully soup! Who tries to poison me?","Vilken hemsk soppa! Vem f�rs�ker f�rgifta mig?");
			ksAddLexicon("mcGirl1","(A young girl's voice) I know what you are supposed to find!",
				"(En ung flickas r�st) Jag vet vad du borde s�ka!");
			ksAddLexicon("mcGirl2","Find recipe and all ingredients for the poisoned pea soup and bring them to the kitchen.",
				"Leta upp recept och alla ingredienser till den f�rgiftade �rtsoppan och l�mna dem till k�ket.");
			ksAddLexicon("mcNoWord","I don't understand what you say.","Jag f�rst�r inte vad du s�ger.");
		}
		
		public override void Enter(Player player)
		{
			girlInRoom = 0;
			totalHallVisits += 1;
									 
			if (totalHallVisits % 3 == 0)
			{
				player.Score += 2;
				player.Tell("mcHallghost" + (totalHallVisits % 3));
				return;
			}
			if (totalHallVisits == 5 || totalHallVisits == 8 || totalHallVisits == 11)
			{
				girlInRoom = 1;
				player.Tell("mcGirl1");
				return;
			}
			base.Enter(player);

		}
		public override void DoTalk(Player player, Command cmd)
		{	
			if (girlInRoom == 1)
			{
				if (cmd[1] == "to" && cmd[2] == "girl" || cmd[1] == "med" && cmd[2] == "flicka")
				{
					player.Score += 3;
					girlInRoom = 2;
					player.Tell("mcGirl2");
					return;
				}
				player.Tell("mcNoWord");
				return;
			}
			base.DoTalk(player,cmd);
		}
	}
}