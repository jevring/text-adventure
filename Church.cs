using System;

namespace DoD.GruppB1
{
	/// <summary>
	/// Summary description for Church.
	/// </summary>
	public class Church : Room
	{
		public Church() : base() {}
		// can we have more players in the same room at the same time? Also, would they have the same instance of the room?
		public override void Enter(Player player)
		{
			base.Enter (player);
			player.Tell("As you enter the church, you see the ceiling rise majestically over head, and the afternoon light falls in through the stained glass windows and makes traces through the airborn dust. The wooden pews are all a bit off, like they've been left in a hurry. To your side is a table with canles, two of which are lit. Down the aisle is the altar, upon which is set a bowl of holywater and some vials.");
			// items: bible, candle, holywater, 3 x vial, "front pew, right leg"
		}
		public void PickupBible(Player player)
		{
			player.Tell("That's merely there as an ornament, leave it where you found it!");
		}
		public void PickupCandle(Player player)
		{
			PickupBible(player);
		}
		public void PickupVial(Player player)
		{
			if (player.GetFromKnapsack("vial") != null)
			{
				player.Tell("The ghosts of this place will not allow you to take more than one vial from here. You put the vial back.");
			}
			else
			{
				player.Tell("You approach the altar. Walking down the aisle you feel the hairs prickle on your neck and your footsteps echo through the entire church. Once you reach the altar you grab one of the vials. As you open it, you seem to hear a slight sigh and the sound of the wind outside. You dit the vial in the bowl and bring it back upp filled. You seal the vial and put it in your knapsack for later use.");
				// are we supposed to have special objects for these items, or is a string description enough?
				// what will the itmes be used for later?
				player.PutInKnapsack("vial", "a vial filled with holywater");
			}
		}
		public void UsePew(Player player)
		{
			player.Tell("You walk over to the first line of pews, nearest the front. Something inside your head tells you \"front pew, right leg\". You bend down and feel the leg of the pew, it's loose.");
			PickupLeg(player);
		}
		public void PickupLeg(Player player)
		{
			player.Tell("You reach down and remove the leg. Once you have it in your hand, you triumphantly reach your hand up in the air, clutching the leg of the pew. You put the leg in your knapsack.");
			player.PutInKnapsack("pewleg", "front pew, right leg");
		}
		public void Use(Player player, string item)
		{
			switch(item) 
			{
				case "leg" :
				case "pew" : UsePew(player); break;
				case "water" :
				case "holywater" :
				case "vial" : PickupVial(player); break;
				case "bible" : PickupBible(player); break;
				case "candle" : PickupCandle(player); break;
				default : PickupBible(player); break;
			}
		}
		public void Pickup(Player player, string item)
		{
			Use(player, item);
		}
	}
}
