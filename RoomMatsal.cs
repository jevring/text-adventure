using DoD;
namespace DoD.GruppB1

{

	public class RoomMatsal : RoomKS
	{
		public static bool sculpture;
		public static bool fork;
		public static bool painting;
		public static bool salt;

		public RoomMatsal (LexiconDelegate lex) : base(lex)
		{
			ksAddLexicon("RoomMatsal1","You are now standing in the doorway to the dining room. It is a beautiful room with a big dining table in the centre of the room. On the walls are paintings of the king and queen. In front of one of the big windows in the room stands a wooden sculpture, you dont recognise who it is suppose to resemble. On the floor you see a fork that some guest must have dropped.","Du står nu i dörröppningen till matsalen. Det är ett vackert rum med ett stort centralt placerat bord. På väggarna finns där tavlor som föreställer kungen och drottningen. Framför ett av rummets stora fönster står det en träskulptur, du känner inte igen vem det ska föreställa. På golvet ser du en gaffel som någon gäst måste ha tappat.");
			ksAddLexicon("RoomMatsal2","You are now standing in the doorway to the dining room. It is a beautiful room with a big dining table in the centre of the room. On the walls are paintings of the king and queen. In front of one of the big windows in the room stands a wooden sculpture, you dont recognise who it is suppose to resemble.","Du står nu i dörröppningen till matsalen. Det är ett vackert rum med ett stort centralt placerat bord. På väggarna finns där tavlor som föreställer kungen och drottningen. Framför ett av rummets stora fönster står det en träskulptur, du känner inte igen vem det ska föreställa.");
			ksAddLexicon("RMMoveSculpture","The sculpture can not be moved","skulpturen kan inte flyttas");
			ksAddLexicon("RMMoveTable","to heavy","för tungt");
			ksAddLexicon("RMCantMove","it can not be moved","den kan inte flyttas");
			ksAddLexicon("RMMovePainting","You move the painting to the side a litle bit, but it doesnt seems to be of any use to you.","Du flyttar tavlan lite åt sidan, men det ser inte ut att vara till någon nytta för dig");
			ksAddLexicon("RMOpenSculpture","I see no hatch. To open it I would need to use a tool","Jag ser ingen lucka. För att öppna den skulle jag behöva använda ett verktyg");
			ksAddLexicon("RMOpenPainting","The painting can not be opened","Tavlan kan inte öppnas");
			ksAddLexicon("RMCantOpen","It can not be opened","Den går inte att öppna");
			ksAddLexicon("RMPictureDestroyed","The painting is already destroyed","tavlan är redan förstörd");
			ksAddLexicon("RMUseForkPainting","You just destroyed the most magnificent painting in the room","Du har just förstört den mest magnifika tavlan i rummet");
			ksAddLexicon("RMUseSawSculpture","You start to cut wood, and shortly after the head of the statue fall of. Inside the statue you see some salt","Du börjar att såga i statyn, kort därefter faller dess huvud av. Inuti statyn ser du nu lite salt");
			ksAddLexicon("RMUseSawFinished","Its already cut opened","den är redan uppsågad");
			ksAddLexicon("RMTakeSculpture","the sculpture is to heavy","sculpturen är för tung");
			ksAddLexicon("RMTakePainting","I am not a thief","Jag är ingen tjuv");
			ksAddLexicon("RMTakeTable","The table is to big","Bordet är för stort");
			ksAddLexicon("RMNoTake","Can not do that","så kan jag inte göra");
			ksAddLexicon("RMUseForkWhat","Use fork on what?","Använda gaffeln på vad?");
			ksAddLexicon("RMNoEffekt","Can not do that","Så kan jag inte göra");
			ksAddLexicon("RMUseSawWhat","Use saw on what?","Använda sågen på vad?");
		}

		public override void Enter(Player player)
		{	

			player.PutInKnapsack("saw",true);

			if(fork != true)
				player.Tell("RoomMatsal1");
			else 
				player.Tell("RoomMatsal2");
			base.Enter(player);
		}


		public override void DoMove(Player player, Command cmd)
		{
			if (cmd[1] == "träskulptur" || cmd[1] == "skulpture" || cmd[1] =="sculpture")
			{
				player.Tell("RMMoveSculpture");
			}
			else if(cmd[1] == "bord" || cmd[1] == "table")
				player.Tell("RMMoveTable");
			else if(cmd[1] == "painting" || cmd[1] == "tavla" || cmd[1] == "tavlor")
				player.Tell("RMMovePainting");
			else 
				player.Tell("RMCantMove");
		}

		
		public override void DoOpen(Player player, Command cmd)
		{
			if (cmd[1] == "träskulptur" || cmd[1] == "skulpture" || cmd[1] =="sculpture")
			{
				player.Tell("RMOpenSculpture");
			}
			else if(cmd[1] == "painting" || cmd[1] == "tavla" || cmd[1] == "tavlor")
			{
				player.Tell("RMOpenPainting");
			}
			else player.Tell("RMCantOpen");

		}

		public override void DoUse(Player player, Command cmd)
		{

			if (player.GetFromKnapsack("fork") != null && cmd[1] == "fork" || cmd[1] == "gaffel")
			{
				if(cmd[2] == "tavla" || cmd[2] =="tavlor" || cmd[2] == "painting" || cmd[2] =="paintings")
				{
					if(painting == true)
					{
						player.Tell("RMPictureDestroyed");
					}
					else
					{
						player.Tell("RMUseForkPainting");
						painting = true;
					}

				}
				else if(cmd[2] == "on" || cmd[2] =="på")
					{
						if(cmd[3] == "painting" || cmd[3] =="tavla" || cmd[3] == "tavlor" || cmd[3] == "paintings")
						{
							if(painting == true)
							{
								player.Tell("RMPictureDestroyed");
							}
							else
							{
								player.Tell("RMUseForkPainting");
								painting = true;
							}
						}
						else
							player.Tell("RMUseForkWhat");
					}
					else
						player.Tell("RMUseForkWhat");
				}

			else if(player.GetFromKnapsack("saw") != null && cmd[1] == "saw" || cmd[1] == "s?g")
			{
				if(cmd[2] == "sculpture" || cmd[2] == "skulpture" || cmd[2] =="träskulptur")
				{
					if(sculpture != true)
					{
						sculpture = true;
						player.Tell("RMUseSawSculpture");
					}
					else
					{
						player.Tell("RMUseSawFinished");
					}
				}
				else if(cmd[2] == "on" || cmd[2] =="på")
				{
					if(cmd[3] == "sculpture" || cmd[3] =="skulptur" || cmd[3] == "träskulptur")
					{
						if(sculpture != true)
						{
							sculpture = true;
							player.Tell("RMUseSawSculpture");
						}
						else
						{
							player.Tell("RMUseSawFinished");
						}

					}
					else player.Tell("RMUseSawWhat");

				}
				else player.Tell("RMUseSawWhat");

			}
			else player.Tell("RMNoEffekt");
		}

		public override void DoTake(Player player, Command cmd)
		{
			if (cmd[1] == "träskulptur" || cmd[1] == "skulpture" || cmd[1] =="sculpture")
			{
				player.Tell("RMTakeSculpture");
			}
			else if (fork != true && cmd[1] == "fork" || cmd[1] == "gaffel")
			{
				fork = true;
				player.PutInKnapsack("fork",true);
			}
			else if(cmd[1] == "painting" || cmd[1] == "tavla" || cmd[1] == "tavlor")
			{
				player.Tell("RMTakePainting");
			}
			else if(cmd[1] == "bord" || cmd[1] == "table")
			{
				player.Tell("RMTakeTable");
			}
			else if(sculpture == true && cmd[1] == "salt" && salt != true)
			{
				player.PutInKnapsack("fork",true);
				salt = true;
			}
			else 
				player.Tell("RMNoTake");
		}
	}
}