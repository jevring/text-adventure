
using System.Collections;
using System;

namespace DoD
{
   public delegate void CommandDelegate(Player player,Command cmd);

	public abstract class World 
	{

		protected ArrayList roomList;
		protected ArrayList CommandDefList;
		protected Room startRoom;
		protected abstract void CreateWorldCommands();
		protected abstract void CreateWorldRooms();

		private string name;
		private Hashtable englishLexicon;
		private Hashtable swedishLexicon;
		private Hashtable synonym2e;
		private Hashtable synonym2s;

		public World(String name)
		{
			Name = name;
			roomList = new ArrayList();
			englishLexicon = new Hashtable();
			swedishLexicon = new Hashtable();
			synonym2e = new Hashtable();
			synonym2s = new Hashtable();

			CommandDefList = new ArrayList();
			CommandDelegate doQuit = new CommandDelegate(DoQuit);
			CommandDelegate doQ    = new CommandDelegate(DoQ);
			CommandDelegate doAt   = new CommandDelegate(DoAt);
			CommandDelegate doHelp = new CommandDelegate(DoCommandHelp);

			CreateCommandDef("quit","HQuit",doQuit, doHelp);
			CreateCommandDef("?","H?",doQ, doHelp);
			CreateCommandDef("@","H@",doAt, doHelp);

			AddLexicon("HQuit","Quits the play","Avslutar spelet");
			AddLexicon("H?","Show current room and current available commands.","Visa nuvarande rum och tillg�ngliga kommandon.");
			AddLexicon("H@","@ n where n = world number. Teleport to world n.",
				"@ n d�r n = nummer p� den v�rld du vill teleportera till.");
			AddLexicon("DontUnderstand","I don't understand","Jag f�rst�r inte!");
			AddLexicon("WorldDontExists","There is no such world", "Den v�rlden finns inte!");
			AddLexicon("InRoom","You are in ","Du �r i ");
			AddLexicon("Commands","Commands are: ","Tillg�ngliga kommandon �r: ");
			AddLexicon("WorldNoStartRoom","The world has no start room. Enter this world is impossible!",
				"Denna v�rld har inget startrum s� den kan inte bes�kas!!");
			AddLexicon("Exits:","Exits:","Utg�ngar;");
			AddSynonym("quit","sluta");
			CreateWorldCommands();
			CreateWorldRooms();
		}
		public string Name
		{
			get{return name;}
			set{name = value;}
		}

		public Room StartRoom
		{
			get{return startRoom;}
			set{startRoom = value;}
		}

		public ArrayList RoomList
		{
			get{return roomList;}
		}

		private Room AddRoom(Room room)
		{
			RoomList.Add(room);
			room.World = this;
			return room;
		}
		
		public Room AddRoom(Room room, string name)
		{
			room.Name = name;
			return AddRoom(room);
		}

		public Room AddRoom(Room room, string ename, string sname)
		{
			room.Name = ename;
			AddLexicon(ename,ename,sname);
			return AddRoom(room);
		}

		protected void AddLexicon(string key, string engelska, string svenska)
		{
			englishLexicon[key] = engelska;
			swedishLexicon[key] = svenska;
		}

		public string Translate(string key, string language)
		{
			Hashtable lexicon;

			if(language == "e")lexicon = englishLexicon;
			else lexicon = swedishLexicon;
			string txt = (string)lexicon[key];
			if(txt != null) return txt;
			return key;
		}

		public string TranslateCommand(string commandWord, string language)
		{

			if(language == "e")return commandWord;
			string txt = (string)synonym2s[commandWord];
			if(txt != null) return txt;
			return commandWord;
		}

		public string TranslateFromPlayer(string word, string language)
		{
			if(language == "e") return word;
			else if(synonym2e[word] != null) return (string)synonym2e[word];
			else return word;
		}

		protected void AddSynonym(string eword, string sword)
		{
			synonym2e[sword] = eword;
			synonym2s[eword] = sword;
		}


	public virtual bool ExecuteNextCommand(Player player) {
		return ExecuteCommand(player,ReadCommand(player));
	}

	protected virtual Command ReadCommand(Player player) {
		return CreateCommand(player, player.Prompt(">"));
	}

	protected virtual bool ExecuteCommand(Player player, Command cmd) {

		if(cmd == null) 
		{
			NoLegalCommand(player, cmd);
			return false;
		}

		else if(cmd[1].Equals("?"))
		{
			if(cmd.Cdef.DoHelp == null) DoCommandHelp(player,cmd);
			else cmd.Cdef.DoHelp(player, cmd);
		}
		else
		{
			cmd.Cdef.DoCommand(player,cmd);  
		}
		return true;
	}
	
	protected virtual void DoCommandHelp(Player player, Command cmd)
	{
		cmd.ShowHelp(player);
	}
	
	protected virtual Command CreateCommand(Player player, ParsedString fromPlayer)
	{
		ArrayList commands = GetCommandDefList(player);	
		if(fromPlayer.Word.Length == 0) return null;
		foreach(CommandDef cdef in commands)
		{
			if(cdef.Name.Equals(fromPlayer.Word[0])) return new Command(cdef,fromPlayer);
		}
		return null;		
	}
	
	protected virtual void CreateCommandDef(String name, string help, CommandDelegate del)
	{
		CreateCommandDef(name, help, del, null);
	}

	protected virtual void CreateCommandDef(String name, string help, CommandDelegate del, CommandDelegate helpdel)
	{
		CommandDef cDef = new CommandDef(name, help, del, helpdel);
		CommandDefList.Add(cDef);	
	}
	
	protected virtual ArrayList GetCommandDefList(Player player)
	{
		return CommandDefList;
	}
	
	protected virtual void NoLegalCommand(Player player, Command cmd)
	{
		player.Tell("DontUnderstand");
	}
	
	public virtual void EnterRoom(Player player, Room room)
	{
		player.Enter(room);
	}

	public virtual void Leave(Player player)
	{
	}
	
	public virtual void Enter(Player player)
	{
	}
		
	public virtual void ChangeWorld(Player player, int n)
	{
		if(n < 0 || n >= Game.WorldList.Count) player.Tell("WorldDontExists");
		else
		{
			player.Enter((World)Game.WorldList[n]);
		}
	}

	protected virtual void DoQuit(Player player, Command cmd)
	{
		player.IsAlive = false;
	}
	
	protected virtual void DoAt(Player player, Command cmd)
	{
	    int n;
		try	{
			n = int.Parse(cmd[1]);			
		}catch(Exception e){n = -1;}
		
		ChangeWorld(player, n);
	}

	protected virtual void DoQ(Player player, Command cmd)
	{
		String comlist = "";
		
		ArrayList commands = GetCommandDefList(player);
		
		foreach(CommandDef cdef in commands)
		{
			comlist += TranslateCommand(cdef.Name, player.Language) + " ";
		}
		
		TellPlayerAtRoomEnter(player, player.Room);
		player.Tell(player.Trans("Commands") + comlist);	
	}
	
		public virtual void TellPlayerAtRoomEnter(Player player, Room room)
		{
			String roomName = player.Trans(room.Name);
			player.Tell(player.Trans("InRoom") + roomName);
			room.ShowExits(player);
		}

}
}
