using DoD;
namespace GruppB1

{

	public class RoomTorture : RoomKS
	{
		static bool getKey;
		static bool openCupboard;
		static bool unlockCupboard;
		static bool moveWheel;
		static bool takeSaw;
		static bool takeStraps;
		static bool takePeas;


		public RoomTorture (LexiconDelegate lex) : base(lex)
		{
			ksAddLexicon("RoomTorture1","As you entering the torture camber you are touched by a gentle breeze with a distinct smell of blood. The room is quiet and quite dark. The camber differs from the rest of the rooms in the castle, for once there isnt any paint on the walls besides some blood stains here and there, and the floor is full of dirt and filth. In the center of the room you see a table with a saw and some cloth straps on it In the back left corner lies what seems to be a human skeleton. Its hands are strapped to a chain which is connected to some sort of metal wheel. On the left wall there is a metal cupboard, and under it there is a broken axe leaning up against the wall. Your think for your self 'someone should really clean this place up.",
				"När du går in i tortyrkammaren möts du av en distinkt lukt av blod. Det är tyst i rummet och ganska mörkt. Kammaren skiljer sig från de övriga rummen, exempelvis så finns det ingen färg på väggarna förutom några blodstänk här och där. Golvet är också fyllt med damm och smuts. I centrum av rummet ser du ett bord med en såg och några tygremsor på. I det bakre vänstra hörnet ligger där vad som ser ut att vara ett mänsikoskelett. Dess händer är fastspända med en kedja som i sin tur är kopplad till något sorts metallhjul. På den västra väggen finns där ett metalskåp, och under skåpet finns där en trasig yxa som lutar mot väggen. Du tänker för dig själv 'någon borde värkligen städa upp här'");
			ksAddLexicon("RoomTorture2","As you entering the torture camber you are touched by a gentle breeze with a distinct smell of blood. The room is quiet and quite dark. The camber differs from the rest of the rooms in the castle, for once there isnt any paint on the walls besides some blood stains here and there, and the floor is full of dirt and filth. In the center of the room you see a table with a saw and on it In the back left corner lies what seems to be a human skeleton. Its hands are strapped to a chain which is connected to some sort of metal wheel. On the left wall there is a metal cupboard, and under it there is a broken axe leaning up against the wall. Your think for your self 'someone should really clean this place up'.",
				"När du går in i tortyrkammaren möts du av en distinkt lukt av blod. Det är tyst i rummet och ganska mörkt. Kammaren skiljer sig från de övriga rummen, exempelvis så finns det ingen färg på väggarna förutom några blodstänk här och där. Golvet är också fyllt med damm och smuts. I centrum av rummet ser du ett bord med en såg på. I det bakre vänstra hörnet ligger där vad som ser ut att vara ett mänsikoskelett. Dess händer är fastspända med en kedja som i sin tur är kopplad till något sorts metallhjul. På den västra väggen finns där ett metallskåp, och under skåpet finns där en trasig yxa som lutar mot väggen. Du tänker för dig själv 'någon borde värkligen städa upp här'");
			ksAddLexicon("RoomTorture3","As you entering the torture camber you are touched by a gentle breeze with a distinct smell of blood. The room is quiet and quite dark. The camber differs from the rest of the rooms in the castle, for once there isnt any paint on the walls besides some blood stains here and there, and the floor is full of dirt and filth. In the center of the room you see a table with some cloth straps on it In the back left corner lies what seems to be a human skeleton. Its hands are strapped to a chain which is connected to some sort of metal wheel. On the left wall there is a metal cupboard, and under it there is a broken axe leaning up against the wall. Your think for your self 'someone should really clean this place up.",
				"När du går in i tortyrkammaren möts du av en distinkt lukt av blod. Det är tyst i rummet och ganska mörkt. Kammaren skiljer sig från de övriga rummen, exempelvis så finns det ingen färg på väggarna förutom några blodstänk här och där. Golvet är också fyllt med damm och smuts. I centrum av rummet ser du ett bord med några tygremsor på. I det bakre vänstra hörnet ligger där vad som ser ut att vara ett mänsikoskelett. Dess händer är fastspända med en kedja som i sin tur är kopplad till något sorts metallhjul. På den västra väggen finns där ett metallskåp, och under skåpet finns där en trasig yxa som lutar mot väggen. Du tänker för dig själv 'någon borde värkligen städa upp här'");
			ksAddLexicon("RoomTorture4","As you entering the torture camber you are touched by a gentle breeze with a distinct smell of blood. The room is quiet and quite dark. The camber differs from the rest of the rooms in the castle, for once there isnt any paint on the walls besides some blood stains here and there, and the floor is full of dirt and filth. In the center of the room you see a table with nothing on it In the back left corner lies what seems to be a human skeleton. Its hands are strapped to a chain which is connected to some sort of metal wheel. On the left wall there is a metal cupboard, and under it there is a broken axe leaning up against the wall. Your think for your self 'someone should really clean this place up.",
				"När du går in i tortyrkammaren möts du av en distinkt lukt av blod. Det är tyst i rummet och ganska mörkt. Kammaren skiljer sig från de övriga rummen, exempelvis så finns det ingen färg på väggarna förutom några blodstänk här och där. Golvet är också fyllt med damm och smuts. I centrum av rummet ser du ett bord. I det bakre vänstra hörnet ligger där vad som ser ut att vara ett mänsikoskelett. Dess händer är fastspända med en kedja som i sin tur är kopplad till något sorts metallhjul. På den västra väggen finns där ett metallskåp, och under skåpet finns där en trasig yxa som lutar mot väggen. Du tänker för dig själv 'någon borde värkligen städa upp här'");
			ksAddLexicon("RTWheelFinished","The metalwheel can not be moved again","metalhjulet kan inte flyttas igen");
			ksAddLexicon("RTWheelMove","The wheel slowly moves aside, and a key drops down to the floor","hjulet flyttar sig sakta åt sidan, och en nyckel faller ner på golvet"); 
			ksAddLexicon("RTNothing","You cant do that","så kan du inte göra");
			ksAddLexicon("RTOpenCupboard","The cupboad opens and inside you see some peas.","metallskåpet öpnar sig och därinne ser du några ärtor");
			ksAddLexicon("RTNeedKey","the cupboard is looked","skåp är låst");
			ksAddLexicon("RTCantOpen","you cant open that!","den kan du inte öppna!");
			ksAddLexicon("RTUnlock","you have unlocked the cupboard","Du har låst upp skåpet");
			ksAddLexicon("RTWhat","use the key on what?","använda nyckeln på vad?");
			ksAddLexicon("RTCanNot","Cant do that","det går inte göra");
			ksAddLexicon("RTKeyTaken","there are no more keys here","Det finns inga fler nycklar här");
			ksAddLexicon("RTNoSaw","there are no saw here","Det finns ingen såg här");
			ksAddLexicon("RTAxe","its to heavy","den är för tung");
			ksAddLexicon("RTSkeleton","disgusting, forget it!","äckligt, det kan du glömma");
			ksAddLexicon("RTChain","Its attached to a metalwheel and can not be removed","Den sitter fast i ett metalhjul och kan inte tas bort");
			ksAddLexicon("RTToheavy","Its to heavy","den är för tung");
			ksAddLexicon("RTItsattached","Its attached to the wall and can not be removed","den sitter fast på väggen och kan inte rubbas");
			ksAddLexicon("RTNocando","You can not take that","Du kan inte ta med dig den");
			ksAddLexicon("RTNoUse","I dont see no use for it in this room","Jag ser ingen användning för den i detta rum");
			ksAddLexicon("RTNoStraps","there are no straps to be taken","det finns inga tygremsor att ta");
			ksAddLexicon("RTIsUnlocked","the cupboard is already unlocked","skåpet är redan upplåst");
			ksAddLexicon("RTNotMoved","it can not be moved","den kan inte flyttas");
			ksAddLexicon("RTIsOpen","The cupboard is already open","skåpet är redan öppet");
		}

		public override void Enter(Player player)
		{	
			if(takeSaw != true && takeStraps != true)
				player.Tell("RoomTorture1");
			if(takeSaw != true && takeStraps == true)
				player.Tell("RoomTorture2");
			if(takeSaw == true && takeStraps != true)
				player.Tell("RoomTorture3");
			if(takeSaw == true && takeStraps == true)
				player.Tell("RoomTorture4");
		
			base.Enter(player);
		}


		public override void DoMove(Player player, Command cmd)
		{
			if (cmd[1] == "wheel" || cmd[1] == "hjul" || cmd[1] =="metalhjul" || cmd[1] =="metalwheel")
			{
				if(moveWheel == true)
				{
					player.Tell("RTWheelFinished");
					return;
				}
				else
				{
					player.Tell("RTWheelMove");
				}	
				moveWheel = true;
				return;
			}
			else if (cmd[1] == "cupboard" || cmd[1] == "skåp" || cmd[1] =="metallskåp")
			{
				player.Tell("RTNotMoved");
			}
			else
				player.Tell("RTNothing");
		}
		
		public override void DoOpen(Player player, Command cmd)
		{
			if (cmd[1] == "cupboard" || cmd[1] == "metallskåp" || cmd[1] =="skåp")
			{
				if(unlockCupboard == true && player.GetFromKnapsack("key") != null && openCupboard !=true)
				{
					openCupboard = true;
					player.Tell("RTOpenCupboard");
				}
				else if(openCupboard)
				{
					player.Tell("RTIsOpen");
				}
				else
				{
					player.Tell("RTNeedKey");
				}
			}
			else
				player.Tell("RTCantOpen");
		}

		public override void DoUse(Player player, Command cmd)
		{
			if (getKey == true && cmd[1] == "key" || cmd[1] == "nyckel")
			{
				if(cmd[2] == "cupboard" || cmd[2] == "metallskåp" || cmd[2] =="skåp")
				{
						
					if(unlockCupboard != true)
					{
						player.Tell("RTUnlock");
						unlockCupboard = true;
					}
					else player.Tell("RTIsUnlocked");
				}
				else if(cmd[2] =="on" || cmd[2] =="på")
				{
					if(cmd[3] =="cupboard" || cmd[3] =="metallskåp" || cmd[3] =="skåp")
					{
						if(unlockCupboard != true)
						{
							player.Tell("RTUnlock");
							unlockCupboard = true;
						}
						else player.Tell("RTIsUnlocked");
					}
					else player.Tell("RTWhat");

				}
				else player.Tell("RTWhat");
			}
			else if(takeSaw == true && cmd[1] == "saw" || cmd[1] == "såg")
			{
				player.Tell("RTNoUse");
			}
			else player.Tell("RTCanNot");
		}

		public override void DoTake(Player player, Command cmd)
		{
			if (cmd[1] == "key" || cmd[1] == "nyckel")
			{
				if(getKey != true && moveWheel == true)
				{
					player.PutInKnapsack("key",true);
					getKey = true;
				}
				else if(getKey)
					player.Tell("RTKeyTaken");
				else player.Tell("RTNocando");
			}
			else if(openCupboard == true && takePeas != true && cmd[1] == "peas" || cmd[1] == "ärtor")
			{
				player.PutInKnapsack("peas",true);
				takePeas = true;
			}

			else if (cmd[1] == "saw" || cmd[1] == "såg")
			{
				if(takeSaw != true)
				{
					player.PutInKnapsack("saw",true);
					takeSaw = true;
				}
				else
					player.Tell("RTNoSaw");
			}
			else if(cmd[1] == "axe" || cmd[1] == "yxa")
			{
				player.Tell("RTAxe");
			}
			else if(cmd[1] == "skelett" || cmd[1] == "skeleton")
			{
				player.Tell("RTSkeleton");
			}
			else if(cmd[1] == "kedja" || cmd[1] == "chain")
			{
				player.Tell("RTChain");
			}
			else if(cmd[1] == "hjul" || cmd[1] == "metallhjul" || cmd[1] =="wheel" || cmd[1] == "metalwheel")
			{
				player.Tell("RTToheavy");
			}
			else if(cmd[1] == "cupboard" || cmd[1] == "skåp" || cmd[1] =="metallskåp")
			{
				player.Tell("RTItsattached");
			}
			else if(cmd[1] == "tygremsom" || cmd[1] == "straps" || cmd[1] =="remsor")
			{
				if(takeStraps != true)
				{
					player.PutInKnapsack("Straps",true);
					takeStraps = true;
				}
				else 
					player.Tell("RTNoStraps");
			}
			else player.Tell("RTNocando");
		}
	}
}