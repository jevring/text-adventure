namespace DoD
{
	public class Command 
	{
		public ParsedString FromPlayer;
		public CommandDef Cdef;
	
		public Command(CommandDef cdef,ParsedString t)	
		{
			FromPlayer = t;
			Cdef = cdef;
		}
	
		public string this[int i]
		{
			get
				{return getWord(i);}
		}

		private string getWord(int i)
		{
			if(i < FromPlayer.Word.Length) return FromPlayer.Word[i];
			else return "";
			
		}
	
		public void ShowHelp(Player player)
		{
			player.Tell(Cdef.Help);
		}
	}
}
